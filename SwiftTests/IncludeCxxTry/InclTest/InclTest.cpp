//
//  InclTest.cpp
//  IncludeCxxTry
//
//  Created by Mila Filipovic on 28/01/2024.
//

#include "InclTest.hpp"

#include <stdio.h>
#include <iostream>

InclTest::InclTest() {
    std::cout << "Created cpp." << std::endl;
}

void InclTest::printMessage() {
    std::cout << "Hello, World! From cpp." << std::endl;
}
