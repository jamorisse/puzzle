//
//  InclTest.hpp
//  IncludeCxxTry
//
//  Created by Mila Filipovic on 28/01/2024.
//

#ifndef InclTest_hpp
#define InclTest_hpp

class InclTest {
public:
    InclTest(); // Constructeur
    void printMessage(); // Méthode pour afficher le message
};

#endif /* InclTest_hpp */
