//
//  IncludeCxxTryApp.swift
//  IncludeCxxTry
//
//  Created by Mila Filipovic on 28/01/2024.
//

import SwiftUI

@main
struct IncludeCxxTryApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
