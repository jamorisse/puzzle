//
//  ContentView.swift
//  IncludeCxx
//
//  Created by Mila Filipovic on 28/01/2024.
//

import SwiftUI
import CxxStdlib

struct ContentView: View {
    var test = InclTest()
    
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, world!")
            Text(main())
            
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
