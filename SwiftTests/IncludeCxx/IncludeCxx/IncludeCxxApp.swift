//
//  IncludeCxxApp.swift
//  IncludeCxx
//
//  Created by Mila Filipovic on 28/01/2024.
//

import SwiftUI

@main
struct IncludeCxxApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
