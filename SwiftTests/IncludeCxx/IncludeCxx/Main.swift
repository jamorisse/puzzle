//
//  Main.swift
//  IncludeCxx
//
//  Created by Mila Filipovic on 28/01/2024.
//

import Foundation

func main() -> String{
    var test = InclTest()
    return String(test.printMessage())
}
