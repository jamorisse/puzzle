//
//  InclTest.cpp
//  IncludeCxxTry
//
//  Created by Mila Filipovic on 28/01/2024.
//

#include "InclTest.hpp"

InclTest::InclTest() {
    std::cout << "Created cpp." << std::endl;
}

std::string InclTest::printMessage() {
    std::cout << "Hello, World! From cpp." << std::endl;
    return "Hello, World! From cpp.";
}
