

from modules.missing_packages import install_missing_packages
install_missing_packages()


import numpy as np
import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk, ImageSequence
from model.unet_model import * #move files to a new module
from torchvision.transforms import Compose, PILToTensor, Grayscale
import matplotlib.pyplot as plt
import time, threading

import os

from modules.AnimatedGIF import *
from modules.crop import Cropping
from modules.debruitage import ImageDenoising
from modules.separate_pieces import SeparatePieces
from modules.resolution_algorithme import *

class PuzzleReconstructionApp:
    def __init__(self, master):
        
        self.master = master
        master.title("Résolution automatique de puzzle")

        self.import_button = tk.Button(master, text="Importer une image", command=self.load_image)
        self.import_button.pack(pady=10)

        self.process_button = tk.Button(master, text="Lancer le traitement", command=self.process_image)
        self.process_button.pack(pady=10)
        
        self.crop_button = tk.Button(master, text="Recadrer l'image", command=self.start_cropping)
        self.crop_button.pack(pady=10)

        self.canvas = tk.Canvas(master, width=1000, height=700)
        self.canvas.pack()

        self.image = None
        self.image_path = None

        self.loading_gif = AnimatedGif(master, os.path.join('GIF_image', 'loading.gif'), 0.03)  # (tkinter.parent, filename, delay between frames)

        self.processing_done = threading.Event()
        
        self.cropping = False


    def load_image(self, is_cropped = False):
        if(not(is_cropped)):
            file_path = filedialog.askopenfilename(filetypes=[("Image files", "*.png *.jpg *.jpeg *.gif *.bmp *.JPG")])
        else:
            file_path = self.image_path
        if file_path:
            image = Image.open(file_path)

            self.image_path = file_path

            max_width = 1000
            max_height = 700

            image.thumbnail((max_width, max_height))

            self.image = ImageTk.PhotoImage(image)

            self.canvas.config(width=self.image.width(), height=self.image.height())

            self.canvas.create_image(0, 0, anchor=tk.NW, image=self.image)

    def evaluate_image(self):
        if self.image_path:
            transform = Compose([lambda z: z.resize((2000,1400)), 
                    Grayscale(3),
                    PILToTensor(),
                    lambda z: z.to(dtype=torch.float32)  # Normalize between -1 and 1
                    ])
            
            model = UNet(3, 3)

            device = torch.device('cpu')
            model.load_state_dict(torch.load(os.path.join("model", "MultiDim_BW_30epoch"), map_location=device)) #unet_BW_5_epoques
            model.eval()

            ref_img = Image.open(self.image_path)

            ref_img = transform(ref_img)

            ref_img = ref_img.unsqueeze(0)

            #ref_img = ref_img.to('cuda')  # Move input tensor to GPU if not already there

            #model = model.to('cuda')  # Move model to GPU if not already there

            new_img = model.forward(ref_img)
            proba = torch.softmax(new_img, dim=1)

            proba_np = proba.cpu().detach().numpy()

            return proba_np

    def process_image(self):
        if self.image:
            self.loading_gif.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
            self.loading_gif.start()

            self.processing_thread = threading.Thread(target=self.process_image_threaded)
            self.processing_thread.start()

    def process_image_threaded(self):
        
        proba_np = self.evaluate_image()

        self.processing_thread = None

        class_indices = np.argmax(proba_np, axis=1)

        img = class_indices[0]
        img_new = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.int32)
        img_new[img == 0] = (255,255,255)   #fond
        img_new[img == 1] = (200,140,187)   #contours
        img_new[img == 2] = (0,0,0)         #pieces
        
        im = Image.fromarray(img_new.astype(np.uint8))
        im.save(os.path.join("images","segmentation_result.png"))
        
        # Utiliser la classe ImageProcessor pour effectuer le traitement supplémentaire
        image_denoising = ImageDenoising()
        image_denoising.main()
        
        img_new = Image.open(os.path.join("images", "modified_segmentation_with_contours.png"))
        
        self.loading_gif.stop()  
        self.loading_gif.place_forget()  

        plt.figure(1, figsize=(20,12))
        plt.imshow(img_new)


        plt.savefig('result.png')
        
        # pieces_separation = SeparatePieces(os.path.join("images", "modified_segmentation_with_contours.png"))
        # pieces_separation.find_pieces()

        self.processing_done.set()
    
    def start_cropping(self):
        cropping_window = Cropping(self.image_path)

        




if __name__ == "__main__":

    root = tk.Tk()
    app = PuzzleReconstructionApp(root)
    root.mainloop()