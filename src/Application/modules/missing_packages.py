#
# There are a lot of imports in the main app,
# this module is meant to install all the missing packages.
#

import subprocess
import importlib.util


def install_missing_packages():

    # List of imports needed for the application
    required_imports = [
        'numpy',
        'tkinter',
        'Pillow', #'PIL',
        'torch',
        'matplotlib',
        'scipy',
        'opencv-python', #'cv2',
        'scikit-image', #'skimage',
        'torchvision',
        'ipython',
    ]
    
    missing_packages = []
    
    for package_name in required_imports:
        spec = importlib.util.find_spec(package_name)
        if spec is None:
            missing_packages.append(package_name)
    
    if missing_packages:        #this will always run and display 'Requirement already satisfied' even when the packages are installed, don't know how to fix it
        print("Installing missing packages...")
        for package in missing_packages:
            subprocess.run(['pip', 'install', package])
        print("Installation complete.")
    else:
        print("All required packages are already installed.")