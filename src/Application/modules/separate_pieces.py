import os
import numpy as np
from PIL import Image
from IPython.display import clear_output
import matplotlib.pyplot as plt

from modules.maths import *
from modules.auxiliars import *


class SeparatePieces:
    
    def __init__(self, image_path):
        self.passing = False
        self.img_to_segment = Image.open(image_path)
        self.nb_pieces = 0

    
    
    def kernel_mean(self, p, size, img):
        size//=2
        x, y = p
        mean = 0
        for i in range(-size, size+1):
            for j in range(-size, size+1):
                if(not(is_inside_img((x+i, y+j), img))):
                    continue
                r, g, b = img.getpixel((x+i, y+j))
                mean+=r+g+b
        return mean

    def next_border(self, p, size, img, prev_d):
        minimum = 255*3*(size*size-1)
        d = prev_d
        angle_to_check = 85
        for i in range(-1, 2):
            for j in range(-1, 2):
                if(i==0 and j==0):
                    continue
                
                p_test = p[0]+i, p[1]+j

                if(not is_inside_img(p_test, img)):
                    continue

                
                if(img.getpixel(p_test)==(255,0,0)):
                    continue
                
                if(self.kernel_mean(p_test, 6, img)>(245*3*6*2)): #+ (125*2)):
                    if(red_mean(p_test, img)<=3/8):
                        angle_to_check = 95
                
                # Vérifier qu'on ne fait pas de demi-tour brusque
                if(angle_vectors(prev_d, (i, j))>angle_to_check):
                    continue
                
                if(is_inside_img(p_test, img)):
                    kernel_value = self.kernel_mean(p_test, 6, img)
                    if(kernel_value<minimum):
                        minimum = kernel_value
                        d = (i, j)
        return d

    def find_next_pixel(self, p, img, prev_d):
        d = self.next_border(p, 6, img, prev_d)
        return (p[0]+d[0], p[1]+d[1]), d

    def find_next_pixel_red(self, p, img, prev_d):
        x, y = p
        for i in range(-1, 2):
            for j in range(-1, 2):
                if(i==0 and j==0):
                    continue
                # Vérifier qu'on ne fait pas de demi-tour brusque
                if(angle_vectors(prev_d, (i, j))>90):
                    continue

                p_test = x+i, y+j
                if(is_inside_img(p_test, img)):
                    if(img.getpixel(p_test)==(255,0,0)):
                        return p_test, (i, j)
        return (0, 0), (0, 0)




    def browse_outline(self, p, img, saved_parts):
        pos, d = self.find_next_pixel(p, img, (1, 0))
        count = 0
        
        x, y = pos
        min_x, max_x, min_y, max_y = x, x, y, y
        while(not(is_finished((x, y), 6 ,img, count))):
            (x, y), d = self.find_next_pixel((x, y), img, d)
            min_x, max_x, min_y, max_y = replace_values_if_needed(min_x, max_x, min_y, max_y, x, x, y, y)
            
            if(is_inside_img((x,y), img)):
                img.putpixel((x, y), (255,0,0))

            
            count+=1

            if self.passing:
                clear_output(wait=True)
                fig, ax = plt.subplots(figsize=(30, 20))
                ax.imshow(img.crop((x-50, y-50, x+50, y+50)), interpolation='nearest')
                plt.show()
                input("Appuyez sur Entrée pour continuer.")

        min_x, max_x, min_y, max_y = replace_values_if_needed(min_x, max_x, min_y, max_y, min_x-6, max_x+6, min_y-6, max_y+6)
        
        img_array = np.array(img)
        img_array[min_y:max_y, min_x:max_x, :] = np.apply_along_axis(replace_non_red_by_white, axis=2, arr=img_array[min_y:max_y, min_x:max_x, :])
        # save arrays to savez
        saved_parts = np.append(saved_parts, img_array[min_y:max_y, min_x:max_x, :])
        img = Image.fromarray(img_array)

        clear_output(wait=True)
        fig, ax = plt.subplots(figsize=(30, 20))
        # Save img as jpg file 
        img.crop((min_x, min_y, max_x, max_y)).save(os.path.join('separated_pieces', 'piece_'+ str(self.nb_pieces) + '.jpg'))
        self.nb_pieces += 1
        ax.imshow(img)#.crop((min_x, min_y, max_x, max_y)), interpolation='nearest')
        plt.show()

        res = input("Appuyez sur Entrée pour continuer.")
        if res=="yes":
            self.passing = True
        if res=="no":
            self.passing = False

        return img

    def find_pieces(self):
        img = self.img_to_segment
        saved_parts = np.array([])
        for i in range(img.size[1]):
            for j in range(img.size[0]):
                if(img.getpixel((i,j))==(0,0,0)):# and check_if_close_outside((i,j), img)!='Not'):
                    #print(check_if_close_outside((i,j), img))
                    #clear_output(wait=True)
                    img = self.browse_outline((i,j), img, saved_parts)
        return img


    #usage: img = find_pieces(img_to_segment)