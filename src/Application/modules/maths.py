import numpy as np
from scipy.spatial import distance

def angle_vectors(A, B):
    dot = np.dot(A, B)
    norma = np.linalg.norm(A)
    normb = np.linalg.norm(B)
    if(norma*normb==0):
        return 360
    return np.rad2deg(np.arccos(dot/(norma*normb)))

def faffine(line, x):
    a, b = line
    return x, a*x + b

def affine_from_2_points(pa, pb):
    xa, ya = pa
    xb, yb = pb
    if xa==xb:
        return None
    a = (yb-ya)/(xb-xa)
    b = yb - (a*xb)
    return a, b

def delta(a, b, c):
    if((b*b) - (4*a*c))<0:
        print(a, b, c, (b*b) - (4*a*c))
    return (b*b) - (4*a*c) 

def compute_2_polynom_solutions(a, b, delta):
    if delta<0:
        return None
    if delta==0:
        return -b/(2*a)
    return (-b-np.sqrt(delta))/(2*a), (-b+np.sqrt(delta))/(2*a)

def intersection_circle_line(line, circle):
    a1, b1, r = circle
    a2, b2 = line
    a = (a2*a2) + 1
    b = 2*((a2*b2)-(a1)-(b1*a2))
    c = (a1*a1) + (b2*b2) - (2*(b1*b2)) + (b1*b1) - (r*r)
    d = delta(a, b, c)
    solutions = compute_2_polynom_solutions(a, b, d)
    if solutions == None:
        return None
    if len(solutions)==1:
        return faffine(a2, b2, solutions)
    return faffine(line, solutions[0]), faffine(line, solutions[1])

# Math : https://www.stashofcode.fr/rotation-dun-point-autour-dun-centre/
def rotate_point_on_circle(original_point, center_circle, angle):
    xB, yB = original_point
    xO, yO = center_circle
    xC = (xB - xO) * np.cos(angle) + (yB - yO) * np.sin(angle) + xO
    yC = - (xB - xO) * np.sin(angle) + (yB - yO) * np.cos(angle) + yO
    return xC, yC

def point_almost_equals(a, b, epsilon):
    xa, ya = a
    xb, yb = b
    if abs(xa-xb)<epsilon and abs(ya-yb)<epsilon:
        return True
    return False

def check_if_very_close(pa, pb, epsilon):
    xa, ya = pa
    xb, yb = pb
    if abs(xa-xb)<epsilon and abs(ya-yb)<epsilon:
        return True
    return False

def find_highest_point_distance(pivot, points):
    da = distance.euclidean(pivot, points[0])
    db = distance.euclidean(pivot, points[1])
    if da>db:
        return 0, da
    elif db>da:
        return 1, db
    return 0, da, -1

def farest_point(a, b, x):
    if distance.euclidean(a, x) > distance.euclidean(b, x):
        return a
    return b
