import os
import numpy as np
from PIL import Image
from IPython.display import clear_output
import matplotlib.pyplot as plt
import time

import sys
sys.path.append('.')
from modules import math, auxiliars

def kernel_mean(p, size, img):
    size//=2
    x, y = p
    mean = 0
    count = 0
    for i in range(-size, size+1):
        for j in range(-size, size+1):
            if(not(auxiliars.is_inside_img((x+i, y+j), img))):
                continue
            r, g, b = img.getpixel((x+i, y+j))
            mean+=r+g+b
            count+=1
    return mean/count

def next_border(p, size, img, prev_d):
    minimum = 255*3*(size*size-1)
    d = prev_d
    angle_to_check = 85
    for i in range(-1, 2):
        for j in range(-1, 2):
            if(i==0 and j==0):
                continue

            p_test = p[0]+i, p[1]+j
            if(not(auxiliars.is_inside_img(p_test, img))):
                continue
            
            if(img.getpixel(p_test)==(255,0,0)):
                continue
            
            if(kernel_mean(p_test, 6, img)>(245*3*6*2)): #+ (125*2)):
                if(auxiliars.red_mean(p_test, img)<=3/8):
                    angle_to_check = 95
            
            # Vérifier qu'on ne fait pas de demi-tour brusque
            if(math.angle_vectors(prev_d, (i, j))>angle_to_check):
                continue
            
            if(auxiliars.is_inside_img(p_test, img)):
                kernel_value = kernel_mean(p_test, 6, img)
                if(kernel_value<minimum):
                    minimum = kernel_value
                    d = (i, j)
    return d

def find_next_pixel(p, img, prev_d):
    d = next_border(p, 6, img, prev_d)
    #if d==prev_d:
    #    print("EQUALS")
    return (p[0]+d[0], p[1]+d[1]), d

def find_next_pixel_red(p, img, prev_d):
    x, y = p
    for i in range(-1, 2):
        for j in range(-1, 2):
            if(i==0 and j==0):
                continue
                
            # Vérifier qu'on ne fait pas de demi-tour brusque
            if(math.angle_vectors(prev_d, (i, j))>90):
                continue

            p_test = x+i, y+j
            if(auxiliars.is_inside_img(p_test, img)):
                if(img.getpixel(p_test)==(255,0,0)):
                    return p_test, (i, j)
    return (0, 0), (0, 0)

v_func = np.vectorize(auxiliars.replace_non_red_by_white)

passing = False 
input_entry = False
precision = 70

def browse_outline(p, img, saved_parts):
    pos, d = find_next_pixel(p, img, (1, 0))
    count = 0
    
    global passing
    if passing:
        fig, ax = plt.subplots(figsize=(30, 20))
        a, b = p
        ax.imshow(img.crop((a-50, b-50, a+50, b+50)), interpolation='nearest')
        plt.show()
        global input_entry
        if input_entry:   
            input("Appuyez sur Entrée pour continuer.")
    
    x, y = pos
    min_x, max_x, min_y, max_y = x, x, y, y
    while(not(auxiliars.is_finished((x, y), 6 ,img, count))):
        (x, y), d = find_next_pixel((x, y), img, d)
        if x<0 or y<0:
            break
        min_x, max_x, min_y, max_y = auxiliars.replace_values_if_needed(min_x, max_x, min_y, max_y, x, x, y, y)
        img.putpixel((x, y), (255,0,0))
        count+=1

        if passing:
            clear_output(wait=True)
            fig, ax = plt.subplots(figsize=(30, 20))
            ax.imshow(img.crop((x-precision, y-precision, x+precision, y+precision)), interpolation='nearest')
            #ax.imshow(img)
            plt.show()
            #str = input("Appuyez sur Entrée pour continuer.")
            #if str=='q':
            #    passing = False

    min_x, max_x, min_y, max_y = auxiliars.replace_values_if_needed(min_x, max_x, min_y, max_y, min_x-6, max_x+6, min_y-6, max_y+6)

    #print("Minx Maxs: ", min_x, max_x, min_y, max_y)

    if min_x<0:
        min_x=0
    
    img_array = np.array(img)
    img_array[min_y:max_y, min_x:max_x, :] = np.apply_along_axis(auxiliars.replace_non_red_by_white, axis=2, arr=img_array[min_y:max_y, min_x:max_x, :])
    # save arrays to savez
    saved_parts = np.append(saved_parts, img_array[min_y:max_y, min_x:max_x, :])
    img = Image.fromarray(img_array)

    clear_output(wait=True)
    fig, ax = plt.subplots(figsize=(30, 20))
    # Save img as jpg file 
    img.crop((min_x, min_y, max_x, max_y)).save('/Users/mila/Documents/M2/PFE/separated_pieces/b_01.jpg')
    ax.imshow(img.crop((min_x, min_y, max_x, max_y)), interpolation='nearest')
    plt.show()
    time.sleep(0.5)

    if input_entry:   
        input("Appuyez sur Entrée pour continuer.")
        if res=="yes":
            passing = True
        if res=="no":
            passing = False

    return img

def find_pieces(img):
    saved_parts = np.array([])
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            #print(i,j)
            if(img.getpixel((i,j))==(0,0,0)):# and check_if_close_outside((i,j), img)!='Not'):
                img = browse_outline((i,j), img, saved_parts)
    return img

