""" AnimatedGIF - a class to show an animated gif without blocking the tkinter mainloop()

Copyright (c) 2016 Ole Jakob Skjelten <olesk@pvv.org>
Released under the terms of the MIT license (https://opensource.org/licenses/MIT) as described in LICENSE.md

"""
import sys
import time

import tkinter as tk  # for Python3


class AnimatedGif(tk.Label):
	"""
	Class to show animated GIF file in a label
	Use start() method to begin animation, and set the stop flag to stop it
	"""
	def __init__(self, root, gif_file, delay=0.04):
		"""
		:param root: tk.parent
		:param gif_file: filename (and path) of animated gif
		:param delay: delay between frames in the gif animation (float)
		"""
		tk.Label.__init__(self, root)
		self.root = root
		self.configure(width=100, height=100,bg='PINK')
		self.gif_file = gif_file
		self.delay = delay  # Animation delay - try low floats, like 0.04 (depends on the gif in question)
		self.stop_event = False  # Thread exit request flag

		self._num = 0

	def start(self):
		""" Starts non-threaded version that we need to manually update() """
		self.start_time = time.time()  # Starting timer
		self.stop_event = False
		self._animate()

	def stop(self):
		""" This stops the after loop that runs the animation, if we are using the after() approach """
		self.stop_event = True

	def _animate(self):
		try:
			self.gif = tk.PhotoImage(file=self.gif_file, format='gif -index {}'.format(self._num))  # Looping through the frames
			self.configure(image=self.gif)
			self._num += 1
		except tk.TclError:  # When we try a frame that doesn't exist, we know we have to start over from zero
			self._num = 0
		if not self.stop_event:    # If the stop flag is set, we don't repeat
			self.root.after(int(self.delay*1000), self._animate)
			