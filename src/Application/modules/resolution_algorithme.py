import matplotlib.pyplot as plt
import numpy as np
import cv2
from skimage.util import invert
from scipy.spatial import distance
import os
import PIL
import math
from IPython.display import clear_output


from modules.maths import *
from modules.drawmath import *
from modules.findcorners import *

#https://theailearner.com/tag/convex-hull-opencv/
def convex_cv_v(img_cv): #Pour récupérer contours et convexity defects pour obtenir coordonéees creux et excroissance des pièces de puzzle
    img_cv = invert(img_cv)
    gray = cv2.cvtColor(img_cv, cv2.COLOR_BGR2GRAY)
    for i in range(gray.shape[0]):
        for j in range(gray.shape[1]):
            if(gray[i,j]>50):
                gray[i,j]=255

    # Find the contours in the image 
    ret, thresh = cv2.threshold(gray, 50, 255,0)
    contours, hierarchy = cv2.findContours(thresh, 2, 1) 
    cnt = contours[0]
    hull = cv2.convexHull(cnt,returnPoints=False,clockwise=False)

    defects = cv2.convexityDefects(cnt,hull)
    
    
    return hull,np.reshape(cnt, (cnt.shape[0], 2)),gray,defects

def centre_gravité(cnt):# Pour algorithme de redressement on cherche le centre de gravité de la pièce de puzzle
    sum_x = 0
    sum_y = 0
    for i in range(len(cnt)):
        sum_x += cnt[i][0]
        sum_y += cnt[i][1]

    centre_x = sum_x/len(cnt)
    centre_y = sum_y/len(cnt)
    return centre_x,centre_y


def outer_lock(outer_locks): #tri des coordonnées des excroissances afin de les réunir par 2 car c"est toujours le cas sur des excroissances

    distances = {}
    for i in range(len(outer_locks)):
        for j in range(i+1, len(outer_locks)):
            distances[(i, j)] = distance.euclidean(outer_locks[i], outer_locks[j])


    sorted_distances = sorted(distances.items(), key=lambda x: x[1])


    sorted_points = []


    cpt = 0
    for pair in sorted_distances:
        if cpt*2 == len(outer_locks):
            break; 
        sorted_points.append([outer_locks[pair[0][0]],outer_locks[pair[0][1]]])
        cpt+=1

    return sorted_points



def corner_or_border(inner_locks,outer_locks): #Déterminer le type de pièce
    if len(inner_locks)+len(outer_locks)==2:
        types = "coin"
    elif len(inner_locks)+len(outer_locks)==3:
        types = "bord"
    else:
        types = "autre"

    return types


def rotation_image(img,cnt): #Pour redresser la pièce de puzzle mais ne fonctionne pas comme souhaiter 
    centre_x,centre_y = centre_gravité(cnt)
    new_img = PIL.Image.fromarray(img)
    angle_rad = math.atan2((new_img.width/2)-centre_y,(new_img.height/2)-centre_x)
    angle_deg = math.degrees(angle_rad)
    return angle_deg,angle_rad


def alpha_test(arr): #mettre en alpha les images pour tout mettre dans une même image
    w,h,c = arr.shape
    for j in range(h):
        for i in range(w):
            if arr[i][j][0]>=235 and arr[i][j][1]>=235 and arr[i][j][2]>=235:
                arr[i][j][3]=0
    return arr


#rotated_img2 = new_img2.rotate(0,fillcolor='white',expand=0) doit être comme ça
def puzzle_reconstitution(rotated_img,rotated_img2): #A possiblement modifier pour ajouter les pièces une par une dans une grance image
    canvas = PIL.Image.new('RGB', (550, 800),'white')

    arr = np.array(rotated_img.convert("RGBA"))
    arr = alpha_test(arr)

    arr2 = np.array(rotated_img2.convert("RGBA"))
    arr2 = alpha_test(arr2)

    new_im = PIL.Image.fromarray(arr).convert("RGBA")
    new_im2 = PIL.Image.fromarray(arr2).convert("RGBA")

    canvas.paste(new_im2)
    canvas.paste(new_im,(30,new_im2.size[1]),new_im)
    return canvas



def place_creux(img,inner_lock): # Déterminer les emplacement des creux dans une pièce de puzzle
    arr=[]
    
    for i in range(len(inner_lock)):
        if inner_lock[i][0] >= img.shape[1]//5 and inner_lock[i][0]<=(img.shape[1]//2)-1 and inner_lock[i][1]>=img.shape[0]//2.8 and inner_lock[i][1]<=img.shape[0]//1.6:
            arr.append(-2) # gauche
        elif inner_lock[i][0]>=img.shape[1]//1.7 and inner_lock[i][1]>=(img.shape[0]//2.9) and inner_lock[i][1]<=img.shape[0]//1.7:
            arr.append(2) # droite
        elif inner_lock[i][1]<=img.shape[0]//2.5:
            arr.append(-1) # haut
        elif inner_lock[i][1]>=img.shape[0]//1.6:
            arr.append(1) # bas

    return arr

def place_excroi(img,outer_lock_sorted): # Déterminer les emplacement des excroissances dans une pièce de puzzle
    arr = []
    for i in range(len(outer_lock_sorted)):
        if outer_lock_sorted[i][0][0]<= img.shape[1]//2 and outer_lock_sorted[i][0][1]>= img.shape[0]//2.85 and outer_lock_sorted[i][0][1]<= img.shape[0]//1.45:
            arr.append(-2) # gauche
        elif outer_lock_sorted[i][0][0]> img.shape[1]//2 and outer_lock_sorted[i][0][1]>= img.shape[0]//2.85 and outer_lock_sorted[i][0][1]<= img.shape[0]//1.45:
            arr.append(2) # droite
        elif outer_lock_sorted[i][0][1] <= img.shape[0]//2:
            arr.append(-1) # haut
        elif outer_lock_sorted[i][0][1]> img.shape[0]//2:
            arr.append(1) # bas
    

    return arr


def trouver_emplacement_restant_autre_bord(creux_bord,ex_bord):#Déterminer l'emplacement de la pièce qui est un bord où l'on peut emboîter une pièce qui va au milieu du puzzle et comment
    val_1 = 1
    val_2 = 2
    arr_emp_restant = []
    
    arr_1 = np.where(abs(ex_bord)==val_1)[0]
    arr_2 = np.where(abs(ex_bord)==val_2)[0]
    arr_3 = np.where(abs(creux_bord)==val_1)[0]
    arr_4 = np.where(abs(creux_bord)==val_2)[0]
    if(len(arr_1)==2):
        arr_emp = ex_bord[np.where(abs(ex_bord)!=val_1)[0]]
        if(len(arr_emp_restant) == 0):
            arr_emp_restant.append((creux_bord[0],"e")) #excroissance à mettre
        else :
            arr_emp_restant.append((ex_bord[arr_emp[0]],"c"))#creu à mettre

    elif(len(arr_2)==2):
        
        arr_emp = ex_bord[np.where(abs(ex_bord)!=val_2)[0]]
        if(len(arr_emp_restant) == 0):
            arr_emp_restant.append((creux_bord[0],"e"))#excroissance à mettre
        else :
            arr_emp_restant.append((ex_bord[arr_emp[0]],"c"))#creu à mettre
    

    elif(len(arr_3)==2):
        arr_emp = creux_bord[np.where(abs(creux_bord)!=val_1)[0]]
        if(len(arr_emp_restant) == 0):
            arr_emp_restant.append((ex_bord[0],"c"))#creu à mettre
        else :
            arr_emp_restant.append((ex_bord[arr_emp[0]],"e"))#excroissance à mettre

    elif(len(arr_4)==2):
        arr_emp = creux_bord[np.where(abs(creux_bord)!=val_2)[0]]
        if(len(arr_emp_restant) == 0):
            
            arr_emp_restant.append((ex_bord[0],"c"))#creu à mettre
        else :
            arr_emp_restant.append((creux_bord[arr_emp[0]],"e"))#excroissance à mettre

    return arr_emp_restant

def trouver_emplacement_restant_coin_bord(creux_bord,ex_bord,creux_coin,ex_coin): #Déterminer si on peut emboiter une pièce de type bord dans une pièce de type creu et comment
    emplacement_bord = trouver_emplacement_restant_autre_bord(creux_bord,ex_bord)[0][0] * -1

    arr_possibilite = []

    emp_creu_ex = np.concatenate((creux_coin,ex_coin))
    emp_bord_coin = emp_creu_ex *-1
    
   
    commun = emp_bord_coin[np.where(emp_bord_coin==emplacement_bord)[0]]
    pas_commun = emp_bord_coin[np.where(emp_bord_coin!=emplacement_bord)[0]]
    if len(commun) != 0:
        ex_ou_creu = pas_commun[0]*-1
        ex_test = ex_coin[np.where(ex_coin==ex_ou_creu)[0]]
        creu_test = creux_coin[np.where(creux_coin==ex_ou_creu)[0]]
        if len(ex_test) != 0:
            
            creu = creux_bord[np.where(creux_bord == pas_commun[0])[0]]
            if len(creu) == 0:
                return 'n'
            else:
                arr_possibilite.append((creu[0],'e'))#bord à mettre sur une excroissance
        elif len(creu_test) != 0:
            ex = ex_bord[np.where(ex_bord == pas_commun[0])[0]]
            if len(ex) == 0:
                return 'n'
            else:
                arr_possibilite.append((ex[0],'c'))#bord à mettre sur un creu
    else:
        return 'n'
    
    return arr_possibilite


def possible_bord_bord(creux_bord_1,creux_bord_2,ex_bord_1,ex_bord_2): #Déterminer si deux pièces de type bord peuvent s'emboiter ensemble et comment
    bord_1 = trouver_emplacement_restant_autre_bord(creux_bord_1,ex_bord_1)[0][0]*-1
    bord_2 = trouver_emplacement_restant_autre_bord(creux_bord_2,ex_bord_2)[0][0]*-1
    possible = []
    if bord_1 != bord_2:
        return possible
    
    
    emp_creu_1 = creux_bord_1[np.where(abs(creux_bord_1)!=bord_1)[0]] 
    emp_creu_2 = creux_bord_2[np.where(abs(creux_bord_2)!=bord_2)[0]]
    emp_ex_1 =ex_bord_1[np.where(abs(ex_bord_1)!=bord_1)[0]]
    emp_ex_2 =ex_bord_2[np.where(abs(ex_bord_2)!=bord_2)[0]]
    
    for i in range(len(emp_creu_1)):
        for j in range(len(emp_ex_2)):
            if emp_creu_1[i]*-1==emp_ex_2[j]:
                possible.append((emp_ex_2[j],'possible_creux'))#emplacement de la nouvelle piece où c'est possible sur un creu

    for i in range(len(emp_ex_1)):
        for j in range(len(emp_creu_2)):
            if emp_ex_1[i]*-1==emp_creu_2[j]:
                possible.append((emp_creu_2[j],'possible_ex'))#emplacement de la nouvelle piece où c'est possible sur une excroissance
    
    return possible #si possible size == 0 alors impossible 


def emplacement_coins_piece(img):# Trouver les coordonnées des 4 coins d'une pièce de puzzle
    linea = 1, 0
    lineb = -1, 500

    _,cnt,_,_ = convex_cv_v(img)
    cornera, cornerb, cornerc, cornerd = find_4_corners_and_show(linea, lineb, cnt, to_draw=(False, None))
    return cornera,cornerb,cornerc,cornerd

def closest_corners(p_ex_ou_creu,img): #savoir quelles coins d'une pièce sont les plus proche du creu/excroissance que l'on veut utiliser pour l'emboiter dans une autre pièce
    two_closest_corners = []
    cornera,cornerb,cornerc,cornerd = emplacement_coins_piece(img)
    corners_arr = [cornera,cornerb,cornerc,cornerd]
    if p_ex_ou_creu == -1:
        y_only = np.array([val[1] for val in corners_arr])
        lowest_y = np.where(y_only<img.shape[0]//2)
        two_closest_corners.append(corners_arr[lowest_y[0][0]])
        two_closest_corners.append(corners_arr[lowest_y[0][1]])
    elif p_ex_ou_creu == 1: 
        y_only = np.array([val[1] for val in corners_arr])
        highest_y = np.where(y_only>img.shape[0]//2)
        two_closest_corners.append(corners_arr[highest_y[0][0]])
        two_closest_corners.append(corners_arr[highest_y[0][1]])
    elif p_ex_ou_creu == -2: 
        x_only = np.array([val[1] for val in corners_arr])
        highest_x = np.where(x_only<img.shape[1]//2)
        two_closest_corners.append(corners_arr[highest_x[0][0]])
        two_closest_corners.append(corners_arr[highest_x[0][1]])
    else :
        x_only = np.array([val[1] for val in corners_arr])
        highest_x = np.where(x_only>img.shape[1]//2)
        two_closest_corners.append(corners_arr[highest_x[0][0]])
        two_closest_corners.append(corners_arr[highest_x[0][1]])
    return two_closest_corners

def affichage_piece(path):# Juste affiche une image que l'on souhaite pour voir comment sont afficher l'emplacement des creux et excroissances
    img = cv2.imread(path)
    hull,cnt,gray,defects = convex_cv_v(img)

    epsilon = 15000 #Déterminer grâce au tableau defects en regardant la profondeur (la 4eme valeur du tableau)
    inner_locks = []
    outer_locks = []
    for i in range(defects.shape[0]):
        s,e,f,d = defects[i,0]
        start = tuple(cnt[s])
        end = tuple(cnt[e])
        far = tuple(cnt[f])
        if d>epsilon and d<25000:
            outer_locks.append(far)
            cv2.circle(img,far,5,[0,0,255],-1)
        if d>25000:
            inner_locks.append(far)
            cv2.circle(img,far,5,[0,255,0],-1)
    
    return img 

def init_première_image(path):#initialiser la première image à emboiter ici le coin est choisi
    dir = os.listdir(path)
    for file in dir:
        str_file = path + '/' + file
        img = cv2.imread(str_file)

        hull,cnt,gray,defects = convex_cv_v(img)

        # calcul position excroissances et creux
        epsilon = 15000
        inner_locks_3 = []
        outer_locks_3 = []
        for i in range(defects.shape[0]):
            s,e,f,d = defects[i,0]
            start = tuple(cnt[s])
            end = tuple(cnt[e])
            far = tuple(cnt[f])
            if d>epsilon and d<25000:
                outer_locks_3.append(far)
                cv2.circle(img,far,5,[0,0,255],-1)
            if d>25000:
                inner_locks_3.append(far)
                cv2.circle(img,far,5,[0,255,0],-1)

        sorted_3 = outer_lock(outer_locks_3)
        if(corner_or_border(inner_locks_3,sorted_3)=="coin"):
            break


    creux_1 = place_creux(img,inner_lock=inner_locks_3)
    excroissance_1 = place_excroi(img,sorted_3)
    types_piece = corner_or_border(inner_locks_3,sorted_3)

    return creux_1,excroissance_1,types_piece,file

def alignement_bord_bord(arr_ex_place,arr_creu_place,creu,ex):#Savoir si deux points de deux pièces de types bord sont alignés
    aligne = []
    place_ex = arr_ex_place[0]
    place_creu = arr_creu_place[0]
    new_point = []
    x_0 = ex[place_ex][0][0]
    y_0 = ex[place_ex][0][1]
    x_1 = ex[place_ex][1][0]
    y_1 = ex[place_ex][1][1]
    if (x_0 >= x_1-20 and x_0 <= x_1+20)or (x_1 >= x_0-20 and x_1 <= x_0+20):
        if y_0 < y_1:
            new_y = ((y_1-y_0)/2)+y_0 
        else :
            new_y = ((y_0-y_1)/2)+y_1 
        new_point.append((ex[place_ex][0][0],new_y))
    elif (y_0 >= y_1-20 and y_0 <= y_1+20)or (y_1 >= y_0-20 and y_1 <= y_0+20):
        if x_0 < x_1:
            new_x = ((x_1-x_0)/2)+x_0 
        else :
            new_x = ((x_0-x_1)/2)+x_1 
        new_point.append((new_x,ex[place_ex][0][1]))
    if(creu[place_creu][0]<=new_point[0][0]+10 and creu[place_creu][0]>= new_point[0][0]-10) or (creu[place_creu][1]<=new_point[0][1]+10 and creu[place_creu][1]>= new_point[0][1]-10):
        aligne.append(("o",place_creu,place_ex))
    else:
        aligne.append("n")
    return aligne

def emplacement(val): #emplacement du creux ou excroissance
    if val == -1:
        return "h"
    elif val == 1: 
        return "b"
    elif val == -2: 
        return "g"
    else :
        return "d"

def alignement(arr_ex_place,arr_creu_place,creu,ex): # Savoir si un creu et une excroissance sont alignés entre une pièce du milieu et un bord
    aligne = []
    comm = arr_ex_place[np.where(arr_ex_place==arr_creu_place*-1)[0]]
    place_ex = np.where(arr_ex_place==comm)[0]
    place_creu = np.where(arr_creu_place==comm*-1)[0]
    new_point = []
    x_0 = ex[place_ex[0]][0][0]
    y_0 = ex[place_ex[0]][0][1]
    x_1 = ex[place_ex[0]][1][0]
    y_1 = ex[place_ex[0]][1][1]
    if (x_0 >= x_1-20 and x_0 <= x_1+20)or (x_1 >= x_0-20 and x_1 <= x_0+20):
        if y_0 < y_1:
            new_y = ((y_1-y_0)/2)+y_0 
        else :
            new_y = ((y_0-y_1)/2)+y_1 
        new_point.append((ex[place_ex[0]][0][0],new_y))
    elif (y_0 >= y_1-20 and y_0 <= y_1+20)or (y_1 >= y_0-20 and y_1 <= y_0+20):
        if x_0 < x_1:
            new_x = ((x_1-x_0)/2)+x_0 
        else :
            new_x = ((x_0-x_1)/2)+x_1 
        new_point.append((new_x,ex[place_ex[0]][0][1]))
    if(creu[place_creu[0]][0]<=new_point[0][0]+10 and creu[place_creu[0]][0]>= new_point[0][0]-10) or (creu[place_creu[0]][1]<=new_point[0][1]+10 and creu[place_creu[0]][1]>= new_point[0][1]-10):
        aligne.append(("o",arr_creu_place[place_creu[0]],arr_ex_place[place_ex[0]]))
    else:
        aligne.append("n")
    return aligne

def alignement_autre(arr_ex_place,arr_creu_place,creu,ex):# Savoir si un creu et une excroissance sont alignés entre deux pièces de type bord
    aligne = []
    comm = arr_ex_place[np.where(arr_ex_place==arr_creu_place*-1)[0]]
    if (len(comm)==0):
        for k in range(len(arr_ex_place)):
            for l in range(len(arr_creu_place)):
                if arr_ex_place[k] == arr_creu_place[l]*-1:
                    comm = np.append(comm,arr_ex_place[k])
        if(len(comm)==0):
            return 'n'
    place_ex = np.where(arr_ex_place==comm)[0]
    place_creu = np.where(arr_creu_place==comm*-1)[0]
    if(len(place_creu)==0):
        for k in range(len(comm)):
            for l in range(len(arr_creu_place)):
                if comm[k]==arr_creu_place[l]*-1:
                    place_creu = np.append(place_creu,arr_creu_place[l])
    new_point = []
    for i in range(len(comm)):
        x_0 = ex[place_ex[i]][0][0]
        y_0 = ex[place_ex[i]][0][1]
        x_1 = ex[place_ex[i]][1][0]
        y_1 = ex[place_ex[i]][1][1]
        if (x_0 >= x_1-20 and x_0 <= x_1+20)or (x_1 >= x_0-20 and x_1 <= x_0+20):
            if y_0 < y_1:
                new_y = ((y_1-y_0)/2)+y_0 
            else :
                new_y = ((y_0-y_1)/2)+y_1 
            new_point.append((ex[place_ex[i]][0][0],new_y))
        elif (y_0 >= y_1-20 and y_0 <= y_1+20)or (y_1 >= y_0-20 and y_1 <= y_0+20):
            if x_0 < x_1:
                new_x = ((x_1-x_0)/2)+x_0 
            else :
                new_x = ((x_0-x_1)/2)+x_1 
            new_point.append((new_x,ex[place_ex[i]][0][1]))
        if(creu[place_creu[i]][0]<=new_point[0][0]+10 and creu[place_creu[i]][0]>= new_point[0][0]-10) or (creu[place_creu[i]][1]<=new_point[0][1]+10 and creu[place_creu[i]][1]>= new_point[0][1]-10):
            aligne.append("o",arr_creu_place[place_creu[i]],arr_ex_place[place_ex[i]])#emplacement du creu
        else:
            aligne.append("n")
    return aligne

def creu_excroi_aligné_autre_autre(creu_1,creu_2,ex_1,ex_2,im1,im2):#déterminer si creu et excroissance sont aligné entre deux pièces du milieu
    aligne = ""
    p_creu_2 = place_creux(im2,creu_2)
    p_ex_2 = place_excroi(im2,ex_2)
    p_creu_1 = place_creux(im1,creu_1)
    p_ex_1 = place_excroi(im1,ex_1)
    aligne = alignement_autre(np.array(p_ex_1),np.array(p_creu_2),creu_2,ex_1) #si o emplacement du creu 2
    aligne_2 = alignement_autre(np.array(p_ex_2),np.array(p_creu_1),creu_1,ex_2)#si o emplacement du creu 1
    return aligne,aligne_2


def creu_excroi_aligné_bord_autre(creu_1,creu_2,ex_1,ex_2,t1,im1,im2):#déterminer si creu et excroissance sont aligné entre bord et une pièce du milieu
    aligne = ""
    p_creu_2 = place_creux(im2,creu_2)
    p_ex_2 = place_excroi(im2,ex_2)
    p_creu_1 = place_creux(im1,creu_1)
    p_ex_1 = place_excroi(im1,ex_1)
    if t1 == "bord":
        place_restant = trouver_emplacement_restant_autre_bord(np.array(p_creu_1),np.array(p_ex_1))
        if place_restant[0][1] == "e": #quand la pièce de base à un creu il faut mettre une excroissance
            aligne = alignement(np.array(p_creu_1),np.array(p_ex_2),creu_1,ex_2)
        else:
            aligne = alignement(np.array(p_creu_2),np.array(p_ex_1),creu_2,ex_1)
    else :
        place_restant = trouver_emplacement_restant_autre_bord(np.array(p_creu_2),np.array(p_ex_2))
        if place_restant[0][1] == "e": #quand la nouvelle pièce à un creu il faut une excroissance sur la pièce de base
            aligne = alignement(np.array(p_creu_2),np.array(p_ex_1),creu_2,ex_1)
        else:
            aligne = alignement(np.array(p_creu_1),np.array(p_ex_2),creu_1,ex_2)
    return aligne

def creu_excroi_aligné_bord_bord(creu_1,creu_2,ex_1,ex_2,im1,im2,f1,f2):#déterminer si creu et excroissance sont aligné entre deux pièces du bord
    aligned = []
    p_creu_2 = place_creux(im2,creu_2)
    p_ex_2 = place_excroi(im2,ex_2)
    p_creu_1 = place_creux(im1,creu_1)
    p_ex_1 = place_excroi(im1,ex_1)
    possibilite = possible_bord_bord(np.array(p_creu_1),np.array(p_creu_2),np.array(p_ex_1),np.array(p_ex_2))#emplacement 2eme piece bord creu/ex
    if len(possibilite)==0:
        return "n"
    for i in range(len(possibilite)):
        place_creu = []
        place_ex=[]
        if possibilite[i][1]=="possible_ex":
            c_place = p_creu_2[np.where(p_creu_2==possibilite[i][0])[0][0]]
            ex_place = p_ex_1[np.where(p_ex_1==possibilite[i][0]*-1)[0][0]]
            place_creu.append(c_place)
            place_ex.append(ex_place)
            aligne = alignement_bord_bord(place_ex,place_creu,creu_2,ex_1)
            if aligne[0] == 'n':
                continue
            aligned.append((aligne,f2,f1))
            
        elif possibilite[i][1]=="possible_creux":
            ex_place= p_ex_2[np.where(p_ex_2==possibilite[i][0])[0][0]]
            c_place = p_creu_1[np.where(p_creu_1==possibilite[i][0]*-1)[0][0]]
            place_creu.append(c_place)
            place_ex.append(ex_place)
            aligne = alignement_bord_bord(place_ex,place_creu,creu_1,ex_2)
            if aligne[0] == 'n':
                continue

            aligned.append((aligne,f1,f2))
    return aligned

def creu_excroi_aligné_bord_coin(creu_1,creu_2,ex_1,ex_2,t1,im1,im2,f1,f2):#déterminer si creu et excroissance sont aligné entre un coin et un bord
    aligned = []
    p_creu_2 = place_creux(im2,creu_2)
    p_ex_2 = place_excroi(im2,ex_2)
    p_creu_1 = place_creux(im1,creu_1)
    p_ex_1 = place_excroi(im1,ex_1)
    if t1=="coin":
        poss = trouver_emplacement_restant_coin_bord(np.array(p_creu_2),np.array(p_ex_2),np.array(p_creu_1),np.array(p_ex_1))# result creu ou ex du bord
    else :
        poss = trouver_emplacement_restant_coin_bord(np.array(p_creu_1),np.array(p_ex_1),np.array(p_creu_2),np.array(p_ex_2))# result creu ou ex du bord
    if poss == 'n':
        return 'n'
    if poss[0][1]=='c':#bord doit aller sur un creu; le bord a une excroissance
        if t1 == "coin":
            arr_creu_place = np.array(place_creux(im1,creu_1))
            arr_ex_place = np.array(place_excroi(im2,ex_2))
        else :
            arr_creu_place = np.array(place_creux(im2,creu_1))
            arr_ex_place = np.array(place_excroi(im1,ex_2))
        aligne = alignement(arr_ex_place,arr_creu_place,creu_1,ex_2)
        if aligne[0] != 'n':
            aligned.append((aligne,f1,f2)) 
        else:
            aligned.append(aligne)
    else:
        if t1 == "coin":
            arr_ex_place = np.array(place_excroi(im1,ex_1))
            arr_creu_place = np.array(place_creux(im2,creu_2))
        else:
            arr_ex_place = np.array(place_excroi(im2,ex_1))
            arr_creu_place = np.array(place_creux(im1,creu_2))
        aligne = alignement(arr_ex_place,arr_creu_place,creu_2,ex_1)
        if aligne[0] != 'n':
            aligned.append((aligne,f2,f1))
        else:
            aligned.append(aligne)
    return aligned

def test_image_emboitement(path,creux_1,excroissance_1,types_piece,base_file): #juste essayer pour voir si deux pièces peuvent s'emboiter et à quelle emplacement
    dir = os.listdir(path)
    epsilon = 15000
    possible_creux = []
    possible_ex = []

    for file in dir:
        if file == base_file or file =="bb.png" or file == "b_a.png":#image de test non fonctionnelle et on veut pas la comparé à elle même
            continue
        str_file = path + '/' + file
        for m in range(4):
            excroi = []
            creux_2 = []
            outer_locks = []
            inner_locks = []
            img = cv2.imread(str_file)
            if m < 3 :
                img = cv2.rotate(img,m) #rotation peut être ajouté sur cette ligne
            hull,cnt,gray,defects = convex_cv_v(img)
            for l in range(defects.shape[0]):
                s,e,f,d = defects[l,0]
                start = tuple(cnt[s])
                end = tuple(cnt[e])
                far = tuple(cnt[f])
                if d>epsilon and d<25000:
                    outer_locks.append(far)
                    cv2.circle(img,far,5,[0,0,255],-1)
                if d>25000:
                    inner_locks.append(far)
                    cv2.circle(img,far,5,[0,255,0],-1)
            
            sortedd = outer_lock(outer_locks)
            creux_2 = place_creux(img,inner_locks)
            excroi = place_excroi(img,sortedd)

            type_p = corner_or_border(inner_locks,sortedd)

            if (types_piece == "autre" and type_p == "coin") or (types_piece == "coin" and type_p=="autre"):
                continue #piece ne pouvant pas s'emboiter dans un puzzle

            
            if (types_piece == "coin" and type_p == "coin") or (types_piece == "coin" and type_p=="coin"):
                continue #piece ne pouvant pas s'emboiter dans un puzzle

            elif (types_piece == "autre" and type_p == "bord") or (types_piece == "bord" and type_p=="autre"):
                if types_piece == "bord":
                    arr = trouver_emplacement_restant_autre_bord(np.array(creux_1),np.array(excroissance_1))
                    if arr[0][1] == "e": #quand la pièce de base à un creu il faut mettre une excroissance
                        for z in range(len(excroi)):
                            if excroi[z]*-1==arr[0][0]:
                                possible_creux.append((emplacement(excroi[z]),file,m))#quand l'excroissance de la nouvelle pièce peut aller sur le creu de l'ancienne  
                    else:
                        for z in range(len(creux_2)):
                            if creux_2[z]*-1==arr[0][0]:
                                possible_ex.append((emplacement(creux_2[z]),file,m))#quand le creux de la nouvelle pièce peut aller sur l'excroissance de l'ancienne
                else:
                    arr = trouver_emplacement_restant_autre_bord(np.array(creux_2),np.array(excroi))
                    if arr[0][1] == "e": 
                        for z in range(len(excroissance_1)):
                            if excroissance_1[z]*-1==arr[0][0]:
                                possible_creux.append((emplacement(excroissance_1[z]),file,m))#quand l'excroissance de la nouvelle pièce peut aller sur le creu de l'ancienne    
                    else:
                        for z in range(len(creux_1)):
                            if creux_1[z]*-1==arr[0][0]:
                                possible_ex.append((emplacement(creux_1[z]),file,m))#quand le creux de la nouvelle pièce peut aller sur l'excroissance de l'ancienne
                continue #piece ne pouvant s'emboiter dans un puzzle que d'une façon

            
            elif (types_piece == "coin" and type_p == "bord") or (types_piece == "bord" and type_p=="coin"):
                if types_piece == "coin": #bord == nouvelle piece
                    poss = trouver_emplacement_restant_coin_bord(np.array(creux_2),np.array(excroi),np.array(creux_1),np.array(excroissance_1))# result creu ou ex du bord
                    if(poss == 'n'):
                        continue
                    if poss[0][1]== 'c': #bord doit aller sur un creu; le bord a une excroissance
                        for y in range(len(creux_1)):
                            if creux_1[y]*-1 == poss[0][0]:
                                possible_creux.append((emplacement(creux_1[y]),file,m))#quand l'excroissance de la nouvelle pièce peut aller sur le creu de l'ancienne 
                                
                    else:
                        for y in range(len(excroissance_1)):
                            if excroissance_1[y]*-1==poss[0][0]:#bord doit aller sur une excroissance; le bord a un creu

                                possible_ex.append((emplacement(excroissance_1[y]),file,m))#quand le creux de la nouvelle pièce peut aller sur l'excroissance de l'ancienne
                               
                else:
                    poss = trouver_emplacement_restant_coin_bord(np.array(creux_1),np.array(excroissance_1),np.array(creux_2),np.array(excroi))# result creu ou ex du bord
                    if(poss == 'n'):
                        continue
                    if poss[0][1]== 'c': #bord doit aller sur un creu
                        for y in range(len(creux_2)):
                            if creux_2[y]*-1 == poss[0][0]:
                                possible_ex.append((emplacement(poss[0][0]),file,m))#quand le creux de la nouvelle pièce peut aller sur l'excroissance de l'ancienne
                    else:
                        for y in range(len(excroi)):
                            if excroi[y]*-1==poss[0][0]:#bord doit aller sur une excroissance
                                possible_creux.append((emplacement(poss[0][0] ),file,m))#quand le creux de la nouvelle pièce peut aller sur l'excroissance de l'ancienne
                continue #piece ne pouvant s'emboiter dans un puzzle que d'une façon


            elif (types_piece == "bord" and type_p == "bord") or (types_piece == "bord" and type_p=="bord"):
                possibilite = possible_bord_bord(np.array(creux_1),np.array(creux_2),np.array(excroissance_1),np.array(excroi))
                if(len(possibilite)==0):
                    continue
                if possibilite[1]== 'possible_creux':
                    possible_creux.append((emplacement(possibilite[0]),file,m))#quand l'excroissance de la nouvelle pièce peut aller sur le creu de l'ancienne 
            
                else:
                    possible_ex.append((emplacement(possibilite[0]),file,m))#quand le creux de la nouvelle pièce peut aller sur l'excroissance de l'ancienne

                continue #piece ne pouvant s'emboiter dans un puzzle que d'une façon

            for j in range(len(creux_1)):
                for k in range(len(excroi)):
                    if creux_1[j]*-1 == excroi[k]:
                        possible_creux.append((emplacement(creux_1[j]),file,m))#quand l'excroissance de la nouvelle pièce peut aller sur le creu de l'ancienne 


            for j in range(len(excroissance_1)):
                for k in range(len(creux_2)):
                    if excroissance_1[j]*-1 == creux_2[k]:
                        possible_ex.append((emplacement(excroissance_1[j]),file,m))#quand le creux de la nouvelle pièce peut aller sur l'excroissance de l'ancienne
                        

    return possible_creux,possible_ex

path = os.path.join('..','images','flash_labels')
"""usage:
creu,excroissance,typed,file = init_première_image(path)
tab_creux,tab_ex = test_image_emboitement(path,creu,excroissance,typed,file)
print(file)
print(tab_creux)
print(tab_ex)
"""