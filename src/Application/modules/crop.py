import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import os

class Cropping:
    def __init__(self, image_path):
        self.root = tk.Toplevel()
        self.root.title("Select Rectangle")

        self.original_image_path = image_path
        self.image = Image.open(self.original_image_path)
        self.image = self.resize_image(self.image)

        self.photo = ImageTk.PhotoImage(self.image)

        self.canvas = tk.Canvas(self.root, width=self.image.width, height=self.image.height)
        self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
        self.canvas.pack()

        self.rect_item = None
        self.rect_coords = None

        self.canvas.bind("<Button-1>", self.on_click)
        self.canvas.bind("<B1-Motion>", self.on_drag)
        self.canvas.bind("<ButtonRelease-1>", self.on_release)

        # Add a button to close the window
        self.close_button = tk.Button(self.root, text="Close", command=self.close_window)
        self.close_button.pack()

    def resize_image(self, image):
        screen_width, screen_height = (1920, 1080)
        image_width, image_height = image.size
        if image_width > screen_width or image_height > screen_height:
            ratio = min(screen_width / image_width, screen_height / image_height)
            new_width = int(image_width * ratio)
            new_height = int(image_height * ratio)
            return image.resize((new_width, new_height))
        return image

    def update_rectangle(self):
        if self.rect_item:
            self.canvas.delete(self.rect_item)
        x0, y0, x1, y1 = self.rect_coords
        self.rect_item = self.canvas.create_rectangle(x0, y0, x1, y1, outline="blue")

    def on_click(self, event):
        if self.rect_item:
            self.canvas.delete(self.rect_item)
        self.x0, self.y0 = event.x, event.y
        self.rect_item = self.canvas.create_rectangle(self.x0, self.y0, self.x0, self.y0, outline="blue")


    def on_drag(self, event):
        self.x1, self.y1 = event.x, event.y
        self.canvas.coords(self.rect_item, self.x0, self.y0, self.x1, self.y1)

    def on_release(self, event):
        x1, y1 = event.x, event.y

        if(x1 != self.x0 and y1 != self.y0):
            self.rect_coords = (self.x0, self.y0, x1, y1)

            # Recalculate coordinates to original image size
            image = Image.open(self.original_image_path)
            image_width, image_height = image.size
            ratio_width = image_width / self.canvas.winfo_width()
            ratio_height = image_height / self.canvas.winfo_height()
            x0_original = int(self.x0 * ratio_width)
            y0_original = int(self.y0 * ratio_height)
            x1_original = int(x1 * ratio_width)
            y1_original = int(y1 * ratio_height)

            # Save the cropped image
            base_name, ext = os.path.splitext(os.path.basename(self.original_image_path))
            cropped_image_path = os.path.join(os.path.dirname(self.original_image_path), f"{base_name}_cropped{ext}")
            if(x0_original > x1_original):
                tmp = x1_original
                x1_original = x0_original
                x0_original = tmp
            if(y0_original > y1_original):
                tmp = y1_original
                y1_original = y0_original
                y0_original = tmp
            cropped_image = image.crop((x0_original, y0_original, x1_original, y1_original))
            
            width, height = cropped_image.size
            if height > width:
                cropped_image = cropped_image.transpose(Image.ROTATE_90)
            
            cropped_image.save(cropped_image_path)

            # Show message box
            messagebox.showinfo("Image Saved", f"The cropped image has been saved as:\n{cropped_image_path}")

    def close_window(self):
        self.root.destroy()
        self.cropping = False
