from scipy.ndimage import binary_opening, binary_closing
import cv2
import matplotlib.pyplot as plt
import numpy as np
from skimage.metrics import structural_similarity as ssim
from skimage import io, color, filters
from os import listdir
import os
from PIL import Image

#TODO : Ajouter le calcul de temps pour chaque partie du programme. Essayer sur différentes tailles de puzzle

## This class provides various image denoising functionalities
class ImageDenoising:
    
    def main(self):
        
        image_base = cv2.imread(os.path.join("images","segmentation_result.png"))
        for kernel_x in range(3,8):
            for kernel_y in range(3,8):
                self.debruitage(image=image_base, kernel_size=(kernel_x, kernel_y))
        image_corrigee = cv2.imread(os.path.join("images","segmentation_result.png"))
        best_result = ""
        best_ssim_score = 0
        path = os.path.join("debruitage_results")
        for image in os.listdir(path):
            best_ssim_score, best_result = self.calc_ssim(image_corrigee, os.path.join(path,image), best_ssim_score, best_result)
        print("The best result is for the image : " + best_result)
        
        self.result_convert(best_result)
        
        self.draw_contours(os.path.join("debruitage_results","modified_segmentation.png"))
        
        # Supprimer tous les fichiers dans le répertoire "debruitage_results"
        for file_name in os.listdir(path):
            os.remove(os.path.join(path, file_name))
        
        return
    
    
    def draw_contours(self, img_path):
        # Charger l'image
        image = cv2.imread(img_path)
        
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        print(np.amin(np.array(image)), np.amax(np.array(image)))

        # Appliquer un seuillage pour binariser l'image
        _, thresholded = cv2.threshold(image_gray, 250, 255, 0)

        # Trouver les contours dans l'image
        contours, _ = cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # Dessiner les contours sur l'image noire
        cv2.drawContours(image, contours, -1, (0, 0, 0), thickness=3)
        im = Image.fromarray(image.astype(np.uint8))
        im.save(os.path.join("images", "modified_segmentation_with_contours.png"))
        
        return


    ## This function gives a pieces conversion from black to grey
    def result_convert(self, img_path):
        
        img = Image.open(img_path)
        img_array = np.array(img)
        img_array[img_array <= 120] = 120
        img_array[img_array > 120] = 255
        im = Image.fromarray(img_array.astype(np.uint8))
        im.save(os.path.join("debruitage_results","modified_segmentation.png"))
        
        return


    ## This function give the SSIM between a image produced with denoising treatment algorithm and a manual denoised image
    def calc_ssim(self, image_base, image, best_ssim_score, image_best_score):
        
        # Charger les deux images
        image_res = io.imread(image)

        # Convertir les images en niveaux de gris si elles ne le sont pas déjà
        image1_gray = color.rgb2gray(image_base)
        image2_gray = color.rgb2gray(image_res)

        # Calculer la similarité structurale
        similarity_index, _ = ssim(image1_gray, image2_gray, full=True, data_range=255)

        # Afficher le résultat
        print(f"Indice de similarité structurale : {similarity_index} " + ", pour l'image : " + image)
        if(similarity_index > best_ssim_score):
            best_ssim_score = similarity_index
            image_best_score = image
            
        return best_ssim_score, image_best_score


    ## This function is a denoising image function
    def debruitage(self, image, kernel_size):
        
        # Définir l'élément structurant pour l'ouverture et la fermeture
        kernel = np.ones((kernel_size), np.uint8)
        kernel_fermeture = np.ones((5,5), np.uint8)

        # Appliquer l'opération d'ouverture
        fermeture = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel_fermeture)

        # Appliquer l'opération de fermeture
        ouverture = cv2.morphologyEx(fermeture, cv2.MORPH_OPEN, kernel)

        # Enregistrer le résultat
        cv2.imwrite(os.path.join('debruitage_results', 'resultat_OV_' + str(kernel_size) + '.png'), ouverture)
        return
