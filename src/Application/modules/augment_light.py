# Numpy
import numpy as np

# Import custom modules
import sys
sys.path.append('.')
from modules import rgb_to_hsv_np

from skimage.transform import resize
from skimage.color import rgb2hsv

def upscaleMask(img, resolution):
    return resize(img, (resolution[0], resolution[1]))

def computeMask(resolution1, resolution2, p):
    mask = np.random.random(size=resolution1[0]*resolution1[1])
    mask = mask > p
    img = np.ones((resolution1[0] * resolution1[1]))
    img[mask] = 0
    return upscaleMask(img.reshape((resolution1[0], resolution1[1])), resolution2)

def perturbatedImage(image):
    mask = computeMask((image.shape[0]//1000, image.shape[1]//1000), image.shape, 0.20)
    mask = np.repeat(mask[:,:,np.newaxis], 3, axis=2)
    hsv_img = rgb2hsv(image)
    hsv_img[:,:,2] += (hsv_img[:,:,2]*mask[:,:,2])
    mask = hsv_img[:,:,2] > 1.0
    hsv_img[:,:,2][mask] = 1.0
    img = rgb_to_hsv_np.hsv_to_rgb(hsv_img)
    return img 
