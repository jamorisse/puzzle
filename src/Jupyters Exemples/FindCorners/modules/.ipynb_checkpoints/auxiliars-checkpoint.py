# Check whether a pixel [p] is inside an image [img]
def is_inside_img(p, img):
    x, y = p
    w, h = img.size
    if x<0 or x>=w or y<0 or y>=h:
        return False
    return True

# Check whether the line border finished drawing
def is_finished(p, size, img, count):
    if(count<1300):
        return
    x, y = p
    size//=2
    count = 0
    for i in range(-size, size):
        for j in range(-size, size):
            if(i==0 and j==0):
                continue
            if(is_inside_img((x+i, y+j),img)):
                if(img.getpixel((x+i, y+j))==(255,0,0)):
                    count+=1
    return count>=(size*2)

# Replace a value (value) with the other value (compared_to) if the comparison (how) is satisfied
def replace_value_if_needed(value, compared_to, how):
    returned = compared_to
    if(how=='inf'):
        if(value<compared_to):
            returned = value
    if(how=='infeg'):
        if(value<=compared_to):
            returned = value
    if(how=='sup'):
        if(value>compared_to):
            returned = value
    if(how=='supeg'):
        if(value>=compared_to):
            returned = value
    return returned

def replace_values_if_needed(min_x, max_x, min_y, max_y, x, y, z, k):
    min_x = replace_value_if_needed(x, min_x, 'infeg')
    max_x = replace_value_if_needed(y, max_x, 'supeg')
    min_y = replace_value_if_needed(z, min_y, 'infeg')
    max_y = replace_value_if_needed(k, max_y, 'supeg')
    return min_x, max_x, min_y, max_y

# Replace a non-red pixel by white ("pure" colors)
def replace_non_red_by_white(a):
    if(a[0]==255 and a[1]==0 and a[2]==0):
        return a
    return (255, 255, 255)

def red_mean(p, img):
    x, y = p
    count = 0
    for i in range(-1, 2):
        for j in range(-1, 2):
            if(i==0 and j==0):
                continue
            if(is_inside_img((x+i, y+j),img)):
                if(img.getpixel((x+i, y+j))==(255,0,0)):
                    count+=1
    return count/8