import matplotlib.pyplot as plt

def draw_line(point1, point2, color='b', text=False):
    x_values = [point1[0], point2[0]]
    y_values = [point1[1], point2[1]]
    plt.plot(x_values, y_values, color, linestyle="-")
    if text:
        plt.text(point1[0]-0.015, point1[1]+0.25, "Point1")
        plt.text(point2[0]-0.050, point2[1]-0.25, "Point2")

def draw_line_ax(point1, point2, ax, color='b', text=False):
    x_values = [point1[0], point2[0]]
    y_values = [point1[1], point2[1]]
    ax.plot(x_values, y_values, color, linestyle="-")
    if text:
        ax.text(point1[0]-0.015, point1[1]+0.25, "Point1")
        ax.text(point2[0]-0.050, point2[1]-0.25, "Point2")