import matplotlib.pyplot as plt
import numpy as np

def draw_outline(outline, ax):
    X = outline[:,0]
    Y = outline[:,1]
    ax.scatter(X, Y, marker='+')
    #plt.axes((np.min(X), np.min(Y), np.max(X)-np.min(X), np.max(Y)-np.min(Y)), facecolor='grey')

def draw_outline_and_circle(outline, circle, ax, r=50, color='g'):
    x, y, radius = circle
    pivot = x, y
    
    #c = plt.Circle(pivot, r, color=color)
    #ax.add_patch(c)
    draw_outline(outline, ax)