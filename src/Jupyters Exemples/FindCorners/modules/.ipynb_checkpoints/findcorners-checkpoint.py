import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output
from scipy.spatial import distance

import sys
sys.path.append('.')
from modules import math, drawmath, draws

# Returns: the intersections between the mathematical affine line and the outline, 
# by means of the very close points
#
# Parameter <line>: is a tuple (a, b) representing the equation ax + b of an affine line
# Parameter <outline>: is a tab of point representing an outline 
# Parameter <epsilon>: is a value used to override the fact that the border is made up of finite (hence spaced) points and that the line can pass through 2 points
def intersection_line_outline(line, outline, epsilon):
    intersection_points = []
    for p in outline:
        x, y = p
        if math.point_almost_equals(p, math.faffine(line, x), epsilon):
            add = True
            for i in range(len(intersection_points)):
                if math.check_if_very_close(intersection_points[i], p, 3):
                    add = False
                    intersection_points[i] = (intersection_points[i]+p)/2
            if add:
                intersection_points.append(p)
    return np.array(intersection_points)

# Returns: the farest intersection point between the mathematical affine line and the discrete outline, 
# from the center of the circle (the choice us using a circle instead of a point directly is for convenience purposes,
# on other parts of the code)
#
# Parameter <line>: is a tuple representing the equation ax + b of an affine line
# Parameter <circle>: is a tuple (a, b, r) representing the equation squared(a-x) + square(b-y) = squares(r) of a circle 
# Parameter <outline>: is a tab of point representing an outline 
def intersection_line_outline_from_point(line, circle, outline):
    x, y, r = circle
    pivot = x, y
    intersections_line = intersection_line_outline(line, outline, 2)
    
    max_dist = 0
    p = pivot
    for i in range(len(intersections_line)):
        for j in range(len(intersections_line)):
            p_max,d =  math.farest_point(intersections_line[i], intersections_line[j], pivot)
            if d > max_dist:
                max_dist = d
                p = p_max
    if p[0] == pivot[0] and p[1] == pivot[1]:
        print("error p = pivot")
    
    return p#math.farest_point(intersections_line[0], intersections_line[1], pivot)
#
def compute_and_draw_line_and_intersections_with_outlines(line, outline, color='b', to_draw=(True, None), coordinates=(0, 500)):
    draw, ax = to_draw
    a, b = coordinates
    intersections = intersection_line_outline(line, outline, 2)
    if draw and ax!=None:
        drawmath.draw_line_ax(math.faffine(line, a), math.faffine(line, b), ax, color)
        ax.scatter(intersections[:,0], intersections[:,1], c='pink', marker='o')
    return intersections

# Returns: the farest intersection point between the line rotated by <deg> degrees and the discrete outline, 
# from the center a point, and the rotated line
#
# Parameter <point>: the point on the circle
# Parameter <center_circle>: the center of the circle used for rotation
# Parameter <deg>: the degree of rotation 
# Parameter <outline>: is a tab of point representing an outline 
# Parameter <color> (optionnal): the color of the lines to draw (blue by default)
# Parameter <draw> (optionnal): a tuple (bool, ax matplotlib), where if bool is True, it will draw (interactively) some behaviour of the algorithm on ax 
def rotate_line_around_point_and_draw(point, center_circle, deg, outline, color='b', to_draw=(True, None)):
    rotated_point = math.rotate_point_on_circle(point, center_circle, deg)
    rotated_line = math.affine_from_2_points(center_circle, rotated_point)
    return compute_and_draw_line_and_intersections_with_outlines(rotated_line, outline, 'r', to_draw=to_draw), rotated_line

# Returns: the next point step on the outline to the corner opposite diagonally to the center of the circle
#
# Parameter <line>: is a tuple (a, b) representing the equation ax + b of an affine line
# Parameter <circle>: is a tuple (a, b, r) representing the equation squared(a-x) + square(b-y) = squares(r) of a circle 
# Parameter <outline>: is a tab of point representing an outline 
# Parameter <draw> (optionnal): a tuple (bool, ax matplotlib), where if bool is True, it will draw (interactively) some behaviour of the algorithm on ax 
def compute_next_step(line, circle, outline, to_draw=(True, None)):
    x, y, r = circle
    pivot = x, y

    draw, ax = to_draw

    intersections_circle = math.intersection_circle_line(line, circle)
    intersection = intersections_circle[0]
    
    intersection_original_line = intersection_line_outline_from_point(line, circle, outline) 

    intersections_rot_left, rotated_line_left = rotate_line_around_point_and_draw(intersection, pivot, -np.pi/500, outline, to_draw=to_draw)
    intersections_rot_right, rotated_line_right = rotate_line_around_point_and_draw(intersection, pivot, np.pi/500, outline, to_draw=to_draw)

    ipl, dpl = math.find_highest_point_distance(pivot, intersections_rot_left)
    ipr, dpr = math.find_highest_point_distance(pivot, intersections_rot_right)

    original_distance = distance.euclidean(pivot, intersection_original_line)

    if draw:
        #c = plt.Circle(pivot, 50, color='g')
        #ax.add_patch(c)
    
        X = outline[:,0]
        Y = outline[:,1]

        #drawmath.draw_line_ax(math.faffine(linea, 0), math.faffine(linea, 500), ax, 'b')
        #drawmath.draw_line_ax(math.faffine(lineb, 0), math.faffine(lineb, 500), ax, 'b')
        
        ax.scatter(X, Y, marker='+')
        ax.scatter(x, y, c='pink')
        xi, yi = intersection_line_outline_from_point(line, circle, outline)
        ax.scatter(xi, yi, c='red')
        ax.scatter(intersections_rot_right[ipr,0], intersections_rot_right[ipr,1], c='red', marker='o')
        ax.scatter(intersections_rot_left[ipl,0], intersections_rot_left[ipl,1], c='red', marker='o')
        plt.show()

        #input("Inside compute_next_step ")
        clear_output(wait=True)
    
    if original_distance>dpr and original_distance>dpl:
        return None #None, intersection_original_line, line
    if dpr>dpl:
        return intersections_rot_right[ipr], rotated_line_right
    elif dpl>=dpr:
        return intersections_rot_left[ipl], rotated_line_left

# Returns: the corner on the outline to the corner opposite diagonally to the center of the circle
#
# Parameter <line>: is a tuple (a, b) representing the equation ax + b of an affine line
# Parameter <circle>: is a tuple (a, b, r) representing the equation squared(a-x) + square(b-y) = squares(r) of a circle 
# Parameter <outline>: is a tab of point representing an outline 
# Parameter <draw> (optionnal): a tuple (bool, ax matplotlib), where if bool is True, it will draw (interactively) some behaviour of the algorithm on ax 
def find_corner(line, circle, outline, to_draw=(False, None)):
    x, y, r = circle
    pivot = x, y

    draw, ax = to_draw

    if draw:
        fig, ax = plt.subplots(figsize=(20, 15))
        ax.set_box_aspect(1)
        ax.invert_yaxis()
        draws.draw_outline_and_circle(outline, circle, ax)
        ax.scatter(x, y, c='pink')
        plt.show()

        input("Display begining of find_corner ")
        clear_output(wait=True)

    if draw:
        fig, ax = plt.subplots(figsize=(20, 15))
        ax.set_box_aspect(1)
        ax.invert_yaxis()
        
    res = compute_next_step(line, circle, outline, to_draw=(draw, ax))
    if res==None:

        if draw:
            draws.draw_outline_and_circle(outline, circle, ax)
            ax.scatter(x, y, c='pink')
            xi, yi = intersection_line_outline_from_point(line, circle, outline)
            ax.scatter(xi, yi, c='red')
            plt.show()
    
            input("First step is equal ")
            clear_output(wait=True)
            
        return intersection_line_outline_from_point(line, circle, outline), line
    intersect, line = res

    if draw:
        fig, ax = plt.subplots(figsize=(20, 15))
        ax.set_box_aspect(1)
        ax.invert_yaxis()
        
    res = compute_next_step(line, circle, outline, to_draw=(draw, ax))
    if res==None:

        if draw:
            draws.draw_outline_and_circle(outline, circle, ax)
            ax.scatter(x, y, c='pink')
            xi, yi = intersect
            ax.scatter(xi, yi, c='red')
            plt.show()
    
            input("Second step is equal ")
            clear_output(wait=True)
        return intersect, line
        
    intersect_next, line = res
    
    while(True):
        intersect = intersect_next

        if draw:
            fig, ax = plt.subplots(figsize=(20, 15))
            ax.set_box_aspect(1)
            ax.invert_yaxis()
            
        res = compute_next_step(line, circle, outline, to_draw=(draw, ax))
        if res==None:
            if draw:
                draws.draw_outline_and_circle(outline, circle, ax)
                ax.scatter(x, y, c='pink')
                xi, yi = intersect_next
                ax.scatter(xi, yi, c='red')
                plt.show()
        
                input("Inside loop step is equal ")
                clear_output(wait=True)
                
            break
        intersect_next, line = res
        
    return intersect_next, line

# Returns: the 4 corners on the outline of the correctly oriented puzzle piece
#
# Parameter <line>: is a tuple (a, b) representing the equation ax + b of an affine line
# Parameter <outline>: is a tab of point representing an outline 
# Parameter <draw> (optionnal): a tuple (bool, ax matplotlib), where if bool is True, it will draw
#(interactively) some behaviour of the algorithm on ax 
def find_4_corners_and_show(linea, lineb, outline, to_draw=(False, None)):
    draw, ax = to_draw
    
    # Original intersections 
    intersectionsa = compute_and_draw_line_and_intersections_with_outlines(linea, outline, to_draw=to_draw)
    intersectionsb = compute_and_draw_line_and_intersections_with_outlines(lineb, outline, to_draw=to_draw)
    
    circlea = intersectionsa[0][0], intersectionsa[0][1], 150
    cornera, nlinea = find_corner(linea, circlea, outline, to_draw=to_draw)
    
    circleb = cornera[0], cornera[1], 150
    cornerb, nlineb = find_corner(nlinea, circleb, outline, to_draw=to_draw)
    
    circlec = intersectionsb[0][0], intersectionsb[0][1], 150
    cornerc, nlinec = find_corner(lineb, circlec, outline, to_draw=to_draw)
    
    circled = cornerc[0], cornerc[1], 150
    cornerd, nlined = find_corner(nlinec, circled, outline, to_draw=to_draw)

    clear_output(wait=True)
    fig, ax = plt.subplots(figsize=(20, 15))
    ax.set_box_aspect(1)
    ax.invert_yaxis()

    to_draw = (draw, ax)

    intersectionsa = compute_and_draw_line_and_intersections_with_outlines(linea, outline, to_draw=to_draw)
    intersectionsb = compute_and_draw_line_and_intersections_with_outlines(lineb, outline, to_draw=to_draw)

    X = outline[:,0]
    Y = outline[:,1]
    plt.scatter(X, Y, marker='+', c='b')
    plt.scatter(cornera[0], cornera[1], c='red', marker='o')
    plt.scatter(cornerb[0], cornerb[1], c='red', marker='o')
    plt.scatter(cornerc[0], cornerc[1], c='red', marker='o')
    plt.scatter(cornerd[0], cornerd[1], c='red', marker='o')

    plt.show()

    return cornera, cornerb, cornerc, cornerd