# Puzzle Solver Application


## Français / French

(c.f. below for english version)

Bienvenue dans l'application Puzzle Solver ! Il s'agit d'un projet open source où nous visons à automatiser le processus de résolution de puzzles en utilisant des techniques de vision par ordinateur et d'apprentissage automatique.

### Fonctionnalités :

- L'application est conçue pour :
  - Charger une photo de pièces de puzzle sur une table à partir d'un fichier choisi.
  - Segmenter l'image du puzzle à l'aide d'un modèle d'IA UNet.
  - Séparer chaque pièce de puzzle en plusieurs images.
  - Associer les pièces de puzzle jusqu'à ce que le puzzle soit complet.
  - Afficher le puzzle entièrement assemblé, en indiquant s'il est complet ou s'il manque des pièces.


### Comment exécuter :

**Remarque : Le code de l'application est fait en Python. Vous devez avoir un interpréteur Python installé sur votre système pour exécuter le code.**

Pour exécuter l'application, vous devez exécuter le fichier `App.py` situé dans le dossier `src/Application`.
Voila comment faire en fonction de votre système d'exploitation :

#### Pour Windows :
1. Ouvrez l'invite de commande en appuyant sur `Win + R`, en tapant `cmd` et en appuyant sur Entrée.
2. Naviguez jusqu'au dossier `src\Application` en utilisant la commande `cd`.
3. Tapez `py App.py` ou `python App.py` et appuyez sur Entrée pour exécuter l'application.

#### Pour macOS et Linux :
1. Ouvrez le Terminal.
2. Naviguez jusqu'au dossier `src/Application` en utilisant la commande `cd`.
3. Tapez `python3 App.py` et appuyez sur Entrée pour exécuter l'application.

La première fois que vous exécutez l'application, l'installation des packages (progiciels) requis peut être un peu longue. Les lancements ultérieurs peuvent afficher que certaines exigences sont déjà satisfaites.

Par défaut, l'image résultante est enregistrée sous le nom de 'result.png'. Vous pouvez modifier ce comportement en modifiant le code dans le fichier `App.py`, qui est la partie principale du programme.

### Remarque :

- Toutes les fonctionnalités ne sont pas parfaites et le code n'est pas très précis car cette application est un prototype. Nous vous encourageons à lire, réutiliser et à améliorer le code.

### Utilisation

Lorsque vous lancez l'application, une fenêtre s'ouvre (attention au courant d'air) avec trois boutons en haut :

![Fenêtre principale](/visuals/Basic_UI.png)

1. **Importer une image** : Ce bouton ouvrira un gestionnaire de fichiers pour vous permettre de choisir une image depuis votre ordinateur.
2. **Lancer le traitement** : Ce bouton lancera le processus principal (cela peut prendre du temps).
3. **Recadrer l'image** : Ce bouton prend l'image que vous avez chargée et ouvre une nouvelle fenêtre pour que vous puissiez la recadrer si certaines parties de l'image sont obsolètes (comme le bord de la table) pour que la segmentation fonctionne mieux. Pour l'instant, cette image est ensuite sauvegardée dans le même répertoire que l'image de base, il faut la re-importer pour qu'elle soit utilisée pour le traitement.

#### cas d'erreurs:
- Si vous rencontrez ce type d'erreure:
`FileNotFoundError: [Errno 2] No such file or directory: 'model/MultiDim_BW_30epoch'` / `Fichier non trouvé, il n'existe pas de fichier ou dossier appelé 'model/MultiDim_BW_30epoch'`
Cela signifie que vous avez lancer l'application depuis le mauvais répertoire, pensez bien a vous déplacer dans le dossier `src/Application` avant de lancer l'application.

-  Processus interrompu
Dans ce cas il est possible que les images soit trop grandes lors de la segmentation et du traitement pour votre matériel. Vous pouvez changer la taille des images utilisé dans le fichier `App.py` à la ligne 75 (e.g. `transform = Compose([lambda z: z.resize((2000,1400))` -> `transform = Compose([lambda z: z.resize((1000,700))`)

### Roadmap

Améliorations possibles :
- Charger plusieurs images à la fois et les considérer comme composant le même puzzle.

Il est également possible de changer le modèle d'IA utilisé (situé dans le repertoire `src/Application/model/`). D'autres model plus ou moins entraîné y sont déjà. Le dataset utilisé pour entraîner le modèle peut être trouver sur la branch `images_dataset` dans le repertoire `images/images_dataset_tables_modif`. Sur la même branch s'y trouve également quelques éxecutables python pour appliqué des filtres aux images (permettant un augmentation des données).


### Auteurs et reconnaissance

- Andrea Millasseau 
- Jason Morisse 
- Matthieu Nass 
- Thomas Thorignac 

**Tous les auteurs des codes provenant d'autres sources sont cités dans les fichiers correspondants.**

### Licence

Ce projet est open source.

### État du projet

Ce projet provient d'un travail étudiant pour un PFE (Projet de Fin d'Étude) de master informatique et ne sera probablement pas mis à jour à l'avenir.


## English

Welcome to the Puzzle Solver Application! This is an open-source project where we aim to automate the process of solving puzzles using computer vision and machine learning techniques.

### Features:
- The application is designed to:
  - Load a photo of puzzle pieces on a table from a chosen file.
  - Segment the puzzle image using a UNet AI model.
  - Separate each puzzle piece into multiple images.
  - Match puzzle pieces until the puzzle is complete.
  - Display the fully assembled puzzle, indicating if it's complete or if pieces are missing.


### How to Run:

**Note: The application is coded in Python. You need to have a Python interpreter installed on your system to execute the code.**

To run the application, you need to execute the `App.py` file located in the `src/Application` folder.
Here's how to do so based on your operating system:

#### For Windows:
1. Open Command Prompt by pressing `Win + R`, typing `cmd`, and pressing Enter.
2. Navigate to the `src\Application` folder using the `cd` command.
3. Type `python App.py` and press Enter to execute the application.

#### For macOS and Linux:
1. Open Terminal.
2. Navigate to the `src/Application` folder using the `cd` command.
3. Type `python3 App.py` and press Enter to execute the application.


The first time you run the app, it may take some time to install required packages. Subsequent launches may display that some requirements are already met.

By default, the resulting image is saved as 'result.png'. You can modify this behavior by changing the code in the file `App.py`, which is the main part of the program.

### Note:

- Functionalities are not all perfect and the code isn't very refined because this application is just a prototype. We encourage you to read, re-use and improve the code.


### Usage

When you launch the app, a window will open (be careful of the draft) with three buttons at the top:

![Main window](/visuals/Basic_UI.png)

1. **Import an image (Importer une image)**: This button will open a file manager for you to choose an image from your computer.
2. **Start the processing (Lancer le traitement)**: This button will start the main process (this may take some time).
3. **Crop image (Recadrer l'image)**: This button takes the image you loaded and opens a new window for you to crop it if some parts of the image are obsolete (like the border of the table) for the segmentation to work best. For now, the image is saved in the same folder as the loaded image, you will need to re-import it for it to be used for the processing.

#### error cases:

- If you encouter this type of error:
`FileNotFoundError: [Errno 2] No such file or directory: 'model/MultiDim_BW_30epoch'`
This means that you started the application from the wrong directory, make sure you go into `src/Application` before launching the App.

- Process interrupted
It is possible that the images are too big during the segmentation and processing for your hardware. You can change the size of the images used in the file `App.py` line 75 (e.g. `transform = Compose([lambda z: z.resize((2000,1400))` -> `transform = Compose([lambda z: z.resize((1000,700))`)


### Roadmap

Possible improvements:
- Load multiple images at once and consider them as composing the same puzzle.

It is possible to change the AI model used (located in the `src/Application/model/` folder). Other trained models are already in this folder. The dataset used to train the model can be found on the branch `images_dataset` in the folder `images/images_dataset_tables_modif`. On the same branch you can also find some python executables to apply filters to images (to possibly augment the data).

### Authors and Acknowledgment

- Andrea Millasseau 
- Jason Morisse 
- Matthieu Nass 
- Thomas Thorignac 

**All authors from code from other sources are cited in the corresponding files.**

### License

This project is open source.

### Project Status

This project is a student work for an end of studies (PFE: Projet de Fin d'Étude) of a master's degree and will likely not be updated in the future.